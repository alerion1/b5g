# Techlab 5G Box Hardware

## Components

- ABS box, used to provide mounting and water-resistance
- Jetson Nano 2GB module, used as the computing unit
- Jetson Nano 2GB carrier board, used to make the module IO accessible using standard connectors and to condition its power
- uSD card 128GB, flashed with Jetpack 4.6.3, situated at the back of the module, opposite the USB ports
- IMX477, 4K camera on the side, linked to the MIPI CSI-2 interface on the Nano board using a MIPI 15 lane 150mm ribbon cable
- 50x10mm fan, used to cool the enclosure, plugged in the fan output of the Nano carrier board
- Cable glands, used to feed power through without water ingress
- Protecting foam, used on cooling air inlet and outlet to prevent water ingress
- GPS NEO-8M, on top cover, used to obtain geo-localization
- GPS UART-USB, used to access GPS NMEA 0183 serial output stream
- Quectel RM500Q-AE, used to connect to 4G/5G networks
- Quectel carrier board, placed in the lid, used to provide a SIM slot and an USB interface to the Quectel module
- 4x 5G antennas, placed in the lid, used to receive and transmit signals
- Power module, used to convert the XT60 input voltage of 8-32V to USB-C for Nano power

## Power

There are two ways to power the 5G box:

- Using the XT60 connector for **8-30 VDC** operation. The cables are color-coded red for positive and black for negative. The DC source should be able to provide at least **15W** of input power.
- Using the USB-C external connector. The USB power brick should be able to provide **3A** of current to prevent throttling during heavy load. 2A capability can also be used if rare throttling is acceptable.

### Power consumption

The 5G box has been benchmarked with various equipement attached:

- Jetson Nano: *2.23W*
- Jetson Nano + GPS: *2.41W*
- Jetson Nano + GPS + 4G low speed: *3.76W*
- Jetson Nano + GPS + 4G low speed + WiFi: *4.51W*
- Jetson Nano + GPS + 4G high speed + video-stream 720p30: *5.82W*
- Jetson Nano + GPS + 4G high speed + video-stream 4kp30: *7.45W*

Using these figures, a battery life can be calculated. For example, a small Brainergy 4S1P LiPo battery of 41.4Wh capacity has a battery life of between **5h** and **18h** depending on use.


## Water resistance

The 5G box is provisionally rated as *IP 42*. It can thus be used during rain, provided it is kept upright. Be mindfull to dry the external connectors thoroughly before storing the box to prevent corrosion of the contacts.

## Mounting

The 5G box has four mounting holes 6mm in diameter spaced by 180mm in length and 74mm in width. The box has a bulk (without camera) of 200x150x100mm.

## GPS

A basic GPS unit similar to the NEO-8M module is provided. Its antenna is a planar GPS antenna and is afixed on the inside of the cover, pointing straight up. The module is accessible through a RS232 to USB adapter connected on an USB port.

## Camera

The 5G box is equipped with a 4K camera based on an IMX477 sensor. This camera can be tilted from 10° tilt up to 90° tilt down (looking straight down). It is water resistant an carries the same IP rating as the rest of the 5G box.

## Connectivity

A few passthrough are used to provide connectivity to the 5G box:
- An USB 3 passthrough is available on the side, nearest the fan cover.
- An HDMI passthrough is available next to it. It has been tested up to 4K60 output.
- A Cat.6 RJ45 passthrough is available on the back.
