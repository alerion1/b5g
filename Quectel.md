# 5G connectivity - Quectel

## Driver install

- Copy the qmi_wwan_q driver to the Jetson Nano
- Decompress to `/usr/src/qmi_wwan_q-1.2.1/`
- Add to dkms using `sudo dkms add -m qmi_wwan_q -v 1.2.1`
- Build with dkms using `sudo dkms build -m qmi_wwan_q -v 1.2.1`
- Install with dkms using `sudo dkms install -m qmi_wwan_q -v 1.2.1`

## Connection manager install

- Copy QConnectManager to the Jetson Nano
- Decompress
- Build quectel-CM using `make` in its folder.

## udev rules

- Create the following udev rule to properly load the quectel USB interfaces. AT commands can be input on the `/dev/quectel-at` serial device.
### /etc/udev/rules.d/98-quectel.rules
```bash
ACTION=="add", SUBSYSTEM=="usb",ATTRS{idVendor}=="2c7c",ATTRS{idProduct}=="0800", RUN+="/sbin/modprobe option" RUN+="/sbin/modprobe qmi_wwan_q" RUN+="/bin/sh -c 'echo 2c7c 0800 > /sys/bus/usb-serial/drivers/option1/new_id'" RUN+="/bin/sh -c 'echo -n $id:1.4 > /sys/bus/usb/drivers/option/unbind'" RUN+="/bin/sh -c 'echo -n $id:1.4 > /sys/bus/usb/drivers/qmi_wwan_q/bind'", TAG+="systemd"
SUBSYSTEM=="tty",ATTRS{idVendor}=="2c7c",ATTRS{idProduct}=="0800",ENV{ID_USB_INTERFACE_NUM}=="02",SYMLINK+="quectel-at"
```

- Quectel connection manager should be the only program managing the Quectel modem:
- Append a directive to the NetworkManager.conf to stop NetworkManager from trying to control the quectel-at.
### /etc/NetworkManager/NetworkManager.conf
```bash
...
[keyfile]
unmanaged-devices=type:gsm
```

## Systemd service

- Disable the ModemManager service using `systemctl disable ModemManager.service`.

- Create the following systemd service to auto-start 5G networking
### /etc/systemd/system/quectel.service
```bash
[Unit]
Description=Quectel LTE and 5G service
BindsTo=dev-quectel\x2dat.device
After=dev-quectel\x2dat.device
After=network.target

[Service]
WorkingDirectory=/home/nano1/quectel-CM
ExecStart=/home/nano1/quectel-CM/quectel-CM -s <apn> -p<pin>
Restart=always
RestartSec=30

[Install]
WantedBy=dev-quectel\x2dat.device
```

- Enable the service using `systemctl enable quectel.service`
