# 5G Box

This repositiory contains documentation and information on the 5G Box.
It is divided into:
- A [hardware](Hardware.md) guide.
- A [5G setup](Quectel.md) guide.

## Author

<antoine.richard@alerion.fr>
